package com.example.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;


public class MainActivity extends AppCompatActivity {

    private RadioGroup rGroupCatsDogs;
    private RadioButton rBtnCatsDogs;
    private RadioGroup rGroupStraw;
    private RadioButton rBtnStraw;
    private RadioGroup rGroup420;
    private RadioButton rBtn420;
    private TextView finalScore;
    private CheckBox checkBox22;
    private CheckBox checkBox4;
    private CheckBox checkBox1000;
    private TextInputEditText inputMike;
    private double score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rGroupCatsDogs = findViewById(R.id.rGroupCatsDogs);
        rGroup420 = findViewById(R.id.rGroup420);
        rGroupStraw = findViewById(R.id.rGroupStraw);
        finalScore = findViewById(R.id.textScore);
        inputMike= findViewById(R.id.inputMike);
        checkBox4 = findViewById(R.id.checkBox4);
        checkBox22 = findViewById(R.id.checkBox22);
        checkBox1000 = findViewById(R.id.checkBox1000);
        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                checkQuiz();
            }
        });

    }

    private void checkQuiz(){
        int radioCatsDogsId = rGroupCatsDogs.getCheckedRadioButtonId();
        rBtnCatsDogs = findViewById(radioCatsDogsId);
        if(rBtnCatsDogs.getText().toString().equalsIgnoreCase("Dogs")){
            score++;
            rBtnCatsDogs.setTextColor(Color.parseColor("#32CD32"));
        }
        else{
            rBtnCatsDogs.setTextColor(Color.parseColor("#8B0000"));
        }
        int radioStrawId = rGroupStraw.getCheckedRadioButtonId();
        rBtnStraw= findViewById(radioStrawId);
        if(rBtnStraw.getText().equals("1")){
            score++;
            rBtnStraw.setTextColor(Color.parseColor("#32CD32"));
        }
        else{
            rBtnStraw.setTextColor(Color.parseColor("#8B0000"));
        }
        if(checkBox4.isChecked()){
            score=score+0.5;
            checkBox4.setTextColor(Color.parseColor("#32CD32"));
        }
        if(checkBox22.isChecked()){
            score=score+0.5;
            checkBox22.setTextColor(Color.parseColor("#32CD32"));
        }
        if(checkBox1000.isChecked()){
            score=score-0.5;
            checkBox1000.setTextColor(Color.parseColor("#8B0000"));
        }
        if(inputMike.getText().toString().equalsIgnoreCase("mike")){
            score++;
            inputMike.setTextColor(Color.parseColor("#32CD32"));
        }
        else{
            inputMike.setTextColor(Color.parseColor("#8B0000"));
        }
        int radio420Id = rGroup420.getCheckedRadioButtonId();
        rBtn420 = findViewById(radio420Id);
        if(rBtn420.getText().toString().equals("24")){
            score++;
            rBtn420.setTextColor(Color.parseColor("#32CD32"));
        }
        else{
            rBtn420.setTextColor(Color.parseColor("#8B0000"));
        }
        if(score < 0){
            score=0;
        }
        finalScore.setText("Score: " + score + "/5.0");
        finalScore.setVisibility(View.VISIBLE);

    }
}